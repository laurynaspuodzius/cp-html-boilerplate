var gulp = require('gulp');
var babelify = require('babelify');
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var path = require('path');

gulp.task('js', function () {
  var bundler = browserify({
    entries: 'src/babel/app.js',
    debug: true
  });
  bundler.transform(babelify, {presets: ["es2015"]});

  bundler.bundle()
      .on('error', function (err) { console.error(err); })
      .pipe(source('app.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('src/js'));
});

gulp.task('sass', function () {
  var s = sass();
  s.on('error',function(e){
    gutil.log(e);
    s.emit('end');
  });

  return gulp.src('src/sass/main.scss')
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(s)
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('src/css'));
});

gulp.task('watch', function () {
  gulp.watch('src/babel/*.js', ['js']);
  gulp.watch('src/sass/*.scss', ['sass']);
});